/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tests;

import java.util.ArrayList;
import java.util.Queue;
import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import semka1.RBTree.RBTree;
import semka1.RBTree.RBTreeNode;

/**
 *
 * @author peter
 */
public class BlackNodesTest {

    /**
     *
     */
    public BlackNodesTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     *
     */
    @Test
    public void RBTreeBlackNodes() {
        int n = 5;
        RBTree<Integer> testTree = new RBTree<Integer>();
        ArrayList<Integer> list = new ArrayList<Integer>(n);
        Random rand = new Random();
        int randomNumber=0;
        
        System.out.println(" --- READY TO INSERT --- ");
        for(int i=1;i<n+1;i++)   {
            randomNumber=rand.nextInt(1000) + 1;
            RBTreeNode<Integer, Integer> item = new RBTreeNode<Integer, Integer>(randomNumber,randomNumber);
            testTree.insert(item);
            list.add(item.getKey());
        System.out.println("vlozil som item "+randomNumber);
        //testTree.levelOrderRoot();
        } 
        testTree.levelOrderRoot();
        Queue<RBTreeNode> leaves = testTree.searchLeaves();
        testTree.getRoot().keyAndColorToString();
        while (!leaves.isEmpty()) {
            RBTreeNode tempNode = leaves.poll();
            int countBlack = 0;
            int count2Reds=0;
            System.out.print("Pocet ciernych z listu " + tempNode.getKey() + ": ");
            while (tempNode != testTree.getRoot()) {
                if (tempNode.isBlack()) {
                    countBlack++;
                }
                if (!tempNode.isBlack() && !tempNode.getParent().isBlack()) count2Reds++;
                tempNode = tempNode.getParent();
            }
            System.out.println(countBlack+1+", pocet dvojitych cervenych z listu: "+count2Reds);

        }
    }
}

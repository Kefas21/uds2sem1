/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tests;

import java.util.ArrayList;
import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import semka1.RBTree.RBTree;
import semka1.RBTree.RBTreeNode;

/**
 *
 * @author peter
 */
public class GeneratorTest {

    /**
     *
     */
    public GeneratorTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //

    /**
     *
     */
    @Test
    public void RBTreeGenerator() {
        int n = 10000;
        RBTree<Integer> testTree = new RBTree<>();
        ArrayList<Integer> list = new ArrayList<>();
        Random rand = new Random();
        int randomNumber = 0;
        
//list.add(692);
//list.add(518);
//list.add(285);
//list.add(867);
//list.add(59);
//list.add(331);
//list.add(860);
//list.add(945);
//list.add(649);
//list.add(364);
//list.add(164);
//list.add(421);
//list.add(410);
//list.add(168);
//list.add(422);
//list.add(84);
//list.add(914);
//list.add(388);
//list.add(232);
//list.add(689);
//
//        System.out.println(" --- READY TO INSERT --- ");
//        for (int i = 0; i < list.size(); i++) {
//            randomNumber = list.get(i);
//            System.out.println("Chystam sa vlozit " + randomNumber);
//            RBTreeNode<Integer, Object> item = new RBTreeNode<>(randomNumber, randomNumber);
//            testTree.insert(item);
//            //list.add(item.getKey());
//            System.out.println("vlozil som item " + randomNumber);
//            testTree.levelOrderRoot();
//        }

        System.out.println(" --- READY TO INSERT --- ");
        for(int i=0;i<n;i++)   {
            randomNumber=rand.nextInt(100000) + 1;
            System.out.println("list.add("+randomNumber+");");
            RBTreeNode<Integer, Object> item = new RBTreeNode<>(randomNumber,randomNumber);
            testTree.insert(item);
            if (!list.contains(item.getKey()))
            list.add(item.getKey());
        //System.out.println("vlozil som item "+randomNumber);
        //testTree.levelOrderRoot();
        //testTree.colorTest();
        }  
        testTree.levelOrderRoot();
        System.out.println("Pocet prvkov v liste je " + list.size());
        testTree.colorTest();

        System.out.println(" --- READY TO DELETE --- ");
        int sizeList=list.size();
        for(int i=0;i<sizeList;i++)  {   
            int listItem=list.get(0);
            System.out.println("Idem zmazat item "+listItem);
            testTree.delete(listItem);
            list.remove(0);            
        //testTree.levelOrderRoot();
        if (i%1000==0)testTree.colorTest();
        }     
        
        testTree.levelOrderRoot();  
        testTree.colorTest();
    }

}

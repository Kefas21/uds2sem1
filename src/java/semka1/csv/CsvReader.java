/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semka1.csv;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import semka1.RBTree.RBTree;
import semka1.RBTree.RBTreeNode;
import semka1.entities.Hospital;
import semka1.entities.HospitalIS;
import semka1.entities.Hospitalization;
import semka1.entities.InsuranceCompany;
import semka1.entities.Patient;
import semka1.entities.StartEndID;

/**
 *
 * @author peter
 */
public class CsvReader {

    //Delimiter used in CSV file
    private static final String COMMA_DELIMITER = ",";

    private RBTree<String> hospitalTree = new RBTree<>();
    private RBTree<String> patientTree = new RBTree<>();
    private RBTree<Integer> insuranceTree = new RBTree<>();

    /**
     *
     */
    public void readPatients() {
        int fname = 0;
        int surname = 1;
        int patientID = 2;
        int birth = 3;
        int insID = 4;
        String fileName = "C:\\UDS\\patients.csv";
        BufferedReader fileReader = null;
        try {
            String line = "";

            //Create the file reader
            fileReader = new BufferedReader(new FileReader(fileName));

            //Read the CSV file header to skip it
            //fileReader.readLine();
            //Read the file line by line starting from the second line
            while ((line = fileReader.readLine()) != null) {
                //Get all tokens available in line
                String[] tokens = line.split(COMMA_DELIMITER);
                if (tokens.length > 0) {
                    //Create a new student object and fill his  data
                    long l = Long.valueOf(tokens[birth]);
                    Date date = new Date(l);
                    Patient pat = new Patient(tokens[fname], tokens[surname], tokens[patientID], date, Integer.parseInt(tokens[insID]));
                    patientTree.insert(new RBTreeNode(pat.getIDnumber(), pat));
                }
            }
        } catch (Exception e) {
            System.out.println("Error in CsvFileReader !!!");
            e.printStackTrace();
        } finally {
            try {
                fileReader.close();
            } catch (IOException e) {
                System.out.println("Error while closing fileReader !!!");
                e.printStackTrace();
            }
        }
    }

    /**
     *
     */
    public void readHospitals() {
        int name = 0;
        String fileName = "C:\\UDS\\hospitals.csv";
        BufferedReader fileReader = null;
        try {
            String line = "";

            //Create the file reader
            fileReader = new BufferedReader(new FileReader(fileName));

            //Read the CSV file header to skip it
            //fileReader.readLine();
            //Read the file line by line starting from the second line
            while ((line = fileReader.readLine()) != null) {
                //Get all tokens available in line
                String[] tokens = line.split(COMMA_DELIMITER);
                if (tokens.length > 0) {
                    //Create a new student object and fill his  data
                    Hospital h = new Hospital(tokens[name]);
                    for (int i = 1; i <= 3; i++) {
                        InsuranceCompany ic = new InsuranceCompany(i);
                        h.getInsuranceTree().insert(new RBTreeNode(ic.getInsID(), ic));
                    }
                    hospitalTree.insert(new RBTreeNode(h.getName(), h));
                }
            }
        } catch (Exception e) {
            System.out.println("Error in CsvFileReader !!!");
            e.printStackTrace();
        } finally {
            try {
                fileReader.close();
            } catch (IOException e) {
                System.out.println("Error while closing fileReader !!!");
                e.printStackTrace();
            }
        }
    }

    /**
     *
     */
    public void readHospits() {
        int start = 0;
        int end = 1;
        int diagnosis = 2;
        int patientID = 3;
        int hospitalName = 4;
        String fileName = "C:\\UDS\\";
        String file = "hospits.csv";

        BufferedReader fileReader = null;

        for (int i = 1; i <= 3; i++) {
            InsuranceCompany ic = new InsuranceCompany(i);
            String path = fileName + i + file;
            try {
                String line = "";

                //Create the file reader
                fileReader = new BufferedReader(new FileReader(path));

                //Read the CSV file header to skip it
                //fileReader.readLine();
                //Read the file line by line starting from the second line
                while ((line = fileReader.readLine()) != null) {
                    //Get all tokens available in line
                    String[] tokens = line.split(COMMA_DELIMITER);
                    if (tokens.length > 0) {
                        //Create a new student object and fill his  data
                        Date s = new Date(Long.parseLong(tokens[start]));
                        Date e = new Date(Long.parseLong(tokens[end]));
                        String pID = tokens[patientID];
                        String hID = tokens[hospitalName];
                        Hospitalization pat = new Hospitalization(s, e, tokens[diagnosis], pID, hID);

                        StartEndID key = new StartEndID(s, e, pID);

                        RBTreeNode patNode = patientTree.find(pID);
                        Patient patVal = (Patient) patNode.getValue();

                        RBTreeNode hNode = hospitalTree.find(hID);
                        Hospital hVal = (Hospital) hNode.getValue();
                        hVal.getHospitTree().insert(new RBTreeNode(key, pat));

                        String nameKey = patVal.getFirstName() + patVal.getSurname() + patVal.getIDnumber();
                        hVal.getPatientTree1().insert(new RBTreeNode(patVal.getIDnumber(), patVal));
                        hVal.getPatientTree2().insert(new RBTreeNode(nameKey, patVal));

                        RBTreeNode icNode = hVal.getInsuranceTree().find(i);
                        InsuranceCompany icVal = (InsuranceCompany) icNode.getValue();
                        boolean control=icVal.getHospitalizationTree().insert(new RBTreeNode(key, pat));

                        ic.getHospitalizationTree().insert(new RBTreeNode(key, pat));
                    }
                }
            } catch (Exception e) {
                System.out.println("Error in CsvFileReader !!!");
                e.printStackTrace();
            } finally {
                try {
                    fileReader.close();
                } catch (IOException e) {
                    System.out.println("Error while closing fileReader !!!");
                    e.printStackTrace();
                }
            }
            insuranceTree.insert(new RBTreeNode(ic.getInsID(), ic));
        }
    }

    /**
     *
     * @return
     */
    public RBTree<String> getHospitalTree() {
        return hospitalTree;
    }

    /**
     *
     * @param hospitalTree
     */
    public void setHospitalTree(RBTree<String> hospitalTree) {
        this.hospitalTree = hospitalTree;
    }

    /**
     *
     * @return
     */
    public RBTree<String> getPatientTree() {
        return patientTree;
    }

    /**
     *
     * @param patientTree
     */
    public void setPatientTree(RBTree<String> patientTree) {
        this.patientTree = patientTree;
    }

    /**
     *
     * @return
     */
    public RBTree<Integer> getInsuranceTree() {
        return insuranceTree;
    }

    /**
     *
     * @param insuranceTree
     */
    public void setInsuranceTree(RBTree<Integer> insuranceTree) {
        this.insuranceTree = insuranceTree;
    }

}

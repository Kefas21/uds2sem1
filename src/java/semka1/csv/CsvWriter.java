/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semka1.csv;

import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;
import semka1.RBTree.RBTree;
import semka1.RBTree.RBTreeNode;
import semka1.entities.Hospital;
import semka1.entities.Hospitalization;
import semka1.entities.InsuranceCompany;
import semka1.entities.Patient;

/**
 *
 * @author peter
 */
public class CsvWriter {

    //Delimiter used in CSV file
    private static final String COMMA_DELIMITER = ",";
    private static final String NEW_LINE_SEPARATOR = "\n";
    private static final String path = "C:\\UDS\\";

    //CSV file header
    //private static final String FILE_HEADER = "id,firstName,lastName,gender,age";

    /**
     *
     * @param t
     */
    public static void writePatients(RBTree t) {

        Queue<RBTreeNode> items = new LinkedList();
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(path + "patients.csv");
            //Write the CSV file header
            //fileWriter.append(FILE_HEADER.toString());			
            //Add a new line separator after the header
            //fileWriter.append(NEW_LINE_SEPARATOR);

            //Write object to the CSV file
            items.add(t.getRoot());
            while (!items.isEmpty()) {
                RBTreeNode cur = items.poll();
                Patient curVal = (Patient) cur.getValue();

                fileWriter.append(curVal.getFirstName());
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append(curVal.getSurname());
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append(curVal.getIDnumber());
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append(String.valueOf(curVal.getBirth().getTime()));
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append(String.valueOf(curVal.getIDinsurance()));
                fileWriter.append(NEW_LINE_SEPARATOR);

                if (cur.getRight() != null) {
                    items.add(cur.getRight());
                }
                if (cur.getLeft() != null) {
                    items.add(cur.getLeft());
                }
            }

            System.out.println("CSV file was created successfully !!!");

        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();
        } finally {

            try {
                fileWriter.flush();
                fileWriter.close();
            } catch (IOException e) {
                System.out.println("Error while flushing/closing fileWriter !!!");
                e.printStackTrace();
            }

        }
    }

    /**
     *
     * @param t
     */
    public static void writeHospitals(RBTree t) {

        Queue<RBTreeNode> items = new LinkedList();
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(path + "hospitals.csv");
            //Write the CSV file header
            //fileWriter.append(FILE_HEADER.toString());			
            //Add a new line separator after the header
            //fileWriter.append(NEW_LINE_SEPARATOR);

            //Write object to the CSV file
            items.add(t.getRoot());
            while (!items.isEmpty()) {
                RBTreeNode cur = items.poll();
                Hospital curVal = (Hospital) cur.getValue();

                fileWriter.append(curVal.getName());
                fileWriter.append(NEW_LINE_SEPARATOR);

                if (cur.getRight() != null) {
                    items.add(cur.getRight());
                }
                if (cur.getLeft() != null) {
                    items.add(cur.getLeft());
                }
            }
            System.out.println("CSV file was created successfully !!!");

        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();
        } finally {

            try {
                fileWriter.flush();
                fileWriter.close();
            } catch (IOException e) {
                System.out.println("Error while flushing/closing fileWriter !!!");
                e.printStackTrace();
            }

        }
    }

    /**
     *
     * @param t
     */
    public static void writeHospits(RBTree t) {
        Queue<RBTreeNode> items = new LinkedList();
        Queue<RBTreeNode> iComp = new LinkedList();
        int counter = 0;
        FileWriter fileWriter = null;
        iComp.add(t.getRoot());

        while (!iComp.isEmpty()) {
            try {
                counter++;
                fileWriter = new FileWriter(path + counter + "hospits.csv");
                //Write the CSV file header
                //fileWriter.append(FILE_HEADER.toString());			
                //Add a new line separator after the header
                //fileWriter.append(NEW_LINE_SEPARATOR);

                RBTreeNode ic = iComp.poll();
                if (ic.getRight()!=null) iComp.add(ic.getRight());
                if (ic.getLeft()!=null) iComp.add(ic.getLeft());
                InsuranceCompany icVal = (InsuranceCompany) ic.getValue();

                //Write object to the CSV file
                items.add(icVal.getHospitalizationTree().getRoot());
                while (!items.isEmpty()) {
                    RBTreeNode cur = items.poll();
                    Hospitalization curVal = (Hospitalization) cur.getValue();

                    fileWriter.append(String.valueOf(curVal.getStart().getTime()));
                    fileWriter.append(COMMA_DELIMITER);
                    fileWriter.append(String.valueOf(curVal.getEnd().getTime()));
                    fileWriter.append(COMMA_DELIMITER);
                    fileWriter.append(curVal.getDiagnosis());
                    fileWriter.append(COMMA_DELIMITER);
                    fileWriter.append(curVal.getPatientID());
                    fileWriter.append(COMMA_DELIMITER);
                    fileWriter.append(curVal.getHospitalName());
                    fileWriter.append(NEW_LINE_SEPARATOR);

                    if (cur.getRight() != null) {
                        items.add(cur.getRight());
                    }
                    if (cur.getLeft() != null) {
                        items.add(cur.getLeft());
                    }
                }
                System.out.println("CSV file was created successfully !!!");

            } catch (Exception e) {
                System.out.println("Error in CsvFileWriter !!!");
                e.printStackTrace();
            } finally {

                try {
                    fileWriter.flush();
                    fileWriter.close();
                } catch (IOException e) {
                    System.out.println("Error while flushing/closing fileWriter !!!");
                    e.printStackTrace();
                }

            }
        }
    }
}

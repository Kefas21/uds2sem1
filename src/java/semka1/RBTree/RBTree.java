/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semka1.RBTree;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;
import semka1.entities.Patient;

/**
 *
 * @author peter
 * @param <T>
 */
public class RBTree<T extends Comparable<T>> {

    //koren inicializovany na null
    private RBTreeNode root = null;

    /**
     *
     */
    public RBTree() {
    }

    /**
     *
     * @param key
     * @return
     */
    public RBTreeNode find(T key) {
        RBTreeNode current = root; //do pomocneho listu priradim root
        while (current != null) { //cyklus ide, az kym nenarazi na prazdny uzol
            if (current.getKey().compareTo(key) == 0) {
                return current; //ak je kluc listu zhodny s hladanym klucom, vrati aktualny list/prvok
            } else if (current.getKey().compareTo(key) < 0) {
                current = current.getRight(); //ak je vacsi, pokracuje pravym synom. Ak mensi, lavym
            } else if (current.getKey().compareTo(key) > 0) {
                current = current.getLeft();
            }
        }
        return null; //ked strom neobsahuje dany kluc, vrati null
    }
    
    /**
     *
     * @param key
     * @return
     */
    public List findMultipleNodes(String key) {
        ArrayList<Patient> found = new ArrayList<>();
        Queue<RBTreeNode> nodes = new LinkedList<>();
        nodes.add(root);
        CharSequence k=key;
        while (!nodes.isEmpty()) { //cyklus ide, az kym nodes nie je prazdne
            RBTreeNode current = nodes.poll();
            if (current.getKey().toString().contains(k)) {
                found.add((Patient) current.getValue());
                if (current.getRight()!=null && current.getRight().getKey().toString().contains(k)) nodes.add(current.getRight());
                if (current.getLeft()!=null && current.getLeft().getKey().toString().contains(k)) nodes.add(current.getLeft());
                //ak je kluc listu zhodny s hladanym klucom, aktualny prvok vlozi do zoznamu
            } else if (current.getKey().toString().compareTo(key) < 0) {
                if (current.getRight()!=null) nodes.add(current.getRight()); //ak je vacsi, pokracuje pravym synom. Ak mensi, lavym
            } else if (current.getKey().toString().compareTo(key) > 0) {
                if (current.getLeft()!=null) nodes.add(current.getLeft());
            }
        }
        return found; //ked strom neobsahuje dany kluc, vrati null
    }

    /**
     *
     * @param item
     * @return
     */
    public boolean insert(RBTreeNode item) {
        if (root == null) {
            item.setBlack(true);
            root = item;
            //item = null;
        } else {
            RBTreeNode current = root;
            while (current != null) { //cyklus bezi, pokial nenarazi na null
                //ak su rovnake kluce?
                if (item.getKey().compareTo(current.getKey()) < 0) {
                    if (current.getLeft() == null) {
                        item.setParent(current);
                        current.setLeft(item);
                        current = null;
                    } else {
                        current = current.getLeft();
                    }
                } else if (item.getKey().compareTo(current.getKey()) > 0) {
                    if (current.getRight() == null) {
                        item.setParent(current);
                        current.setRight(item);
                        current = null;
                    } else {
                        current = current.getRight();
                    }
                } else if (item.getKey().compareTo(current.getKey()) == 0) {
                    return false;
                }
            }
            checkTwoRedNodes(item);
        }
        return true;
    }

    /**
     *
     * @param item
     */
    public void checkTwoRedNodes(RBTreeNode item) {
        while (!item.isBlack() && item!=root && !item.getParent().isBlack() && item.getParent()!=root){
        if ((!item.getParent().hasBrother() || item.getParent().getBrother().isBlack()) && !item.isBlack() && !item.getParent().isBlack()) {
            fixTree(item);
        } else if (item.getParent().hasBrother() && !item.getParent().getBrother().isBlack() && !item.isBlack() && !item.getParent().isBlack()) {
            RBTreeNode itemU = item.getParent().getParent();

        item.getParent().setBlack(true);
        item.getParent().getBrother().setBlack(true);

        if (itemU != root) {
            itemU.setBlack(false);
        } else if (itemU == root) {
            itemU.setBlack(true);
        }

        //if (itemU!=root) checkTwoRedNodes(itemU);
            if (itemU!=root && !itemU.getParent().isBlack())
                item=itemU;
            //checkTwoRedNodes(itemU);
            //itemU=itemU.getParent();
        
        }
        }
    }

    /**
     *
     * @param item
     */
    public void fixTree(RBTreeNode item) {
        RBTreeNode vParent = item.getParent();
        RBTreeNode uParent = vParent.getParent();
        RBTreeNode tempChild=null;
        if (item.isRightChild()) { //item je pravym synom
            if (vParent.isLeftChild()) { //vRodic je lavym synom U, uprava podstromu
                tempChild = item.getLeft();
                uParent.setLeft(item);
                item.setParent(uParent);
                item.setLeft(vParent);
                vParent.setParent(item);
                vParent.setRight(tempChild);
                if (tempChild!=null) tempChild.setParent(vParent);
                rotateRight(uParent, false);
            } else if (vParent.isRightChild()) {
                rotateLeft(item.getParent().getParent(), false);
            }
        } else if (item.isLeftChild()) { //item je lavym synom
            if (vParent.isRightChild()) { //vRodic je pravym synom U, uprava podstromu
                tempChild = item.getRight();
                uParent.setRight(item);
                item.setParent(uParent);
                item.setRight(vParent);
                vParent.setParent(item);
                vParent.setLeft(tempChild);
                if (tempChild!=null) tempChild.setParent(vParent);
                rotateLeft(uParent, false);
            } else if (vParent.isLeftChild()) {
                rotateRight(item.getParent().getParent(), false);
            }
        }
    }

    /**
     *
     * @param item
     * @param deleting
     */
    public void rotateLeft(RBTreeNode item, boolean deleting) {
        RBTreeNode tempItem = item.getRight().getLeft();
        if (item != root) {
            item.getRight().setParent(item.getParent());
            if (item.isLeftChild()) {
                item.getParent().setLeft(item.getRight());
            } else if (item.isRightChild()) {
                item.getParent().setRight(item.getRight());
            }
        } else {
            item.getRight().setParent(null);
            this.root = item.getRight();
        }

        item.setParent(item.getRight());
        item.getRight().setLeft(item);
        if (tempItem != null) {
            item.setRight(tempItem);
            item.getRight().setParent(item);
        } //itemA prevezme lavy podstrom uzla B do pravej vetvy
        else {
            item.setRight(null);
        }

        if (!deleting) {
            item.getParent().setBlack(true);
            if (item.hasBrother()) {
                item.getBrother().setBlack(false);
            }
            item.setBlack(false);
            if (item.getRight() != null) {
                item.getRight().setBlack(true);
            }
        }
    }

    /**
     *
     * @param item
     * @param deleting
     */
    public void rotateRight(RBTreeNode item, boolean deleting) {
        RBTreeNode tempItem = item.getLeft().getRight();
        if (item != root) {
            item.getLeft().setParent(item.getParent());
            if (item.isLeftChild()) {
                item.getParent().setLeft(item.getLeft());
            } else if (item.isRightChild()) {
                item.getParent().setRight(item.getLeft());
            }
        } else {
            item.getLeft().setParent(null);
            this.root = item.getLeft();
        }

        item.setParent(item.getLeft());
        item.getLeft().setRight(item);
        if (tempItem != null) {
            item.setLeft(tempItem);
            item.getLeft().setParent(item);
        } //itemA prevezme pravy podstrom uzla B do lavej vetvy
        else {
            item.setLeft(null);
        }

        if (!deleting) {
            item.getParent().setBlack(true);
            if (item.hasBrother()) {
                item.getBrother().setBlack(false);
            }
            item.setBlack(false);
            if (item.getLeft() != null) {
                item.getLeft().setBlack(true);
            }
        }
    }

    /**
     *
     * @param item
     */
    public void levelOrder(RBTreeNode item) {
        if (root == null) {
            System.out.println("Strom nenajdeny / root je null");
            return;
        }
        int RBTsize = 0;
        Queue<RBTreeNode> queue = new LinkedList<RBTreeNode>();
        queue.add(item);
        while (!queue.isEmpty()) {
            RBTreeNode tempNode = queue.poll();
            RBTsize++;
            tempNode.keyAndColorToString();
            if (tempNode.getLeft() != null) {
                queue.add(tempNode.getLeft());
            }
            if (tempNode.getRight() != null) {
                queue.add(tempNode.getRight());
            }
        }
        System.out.println("Pocet prvkov RB stromu je " + RBTsize);
    }

    /**
     *
     * @return
     */
    public Queue searchLeaves() {
        if (this.root == null) {
            System.out.println("Strom nenajdeny / root je null");
            return null;
        }
        Queue<RBTreeNode> queue = new LinkedList<RBTreeNode>();
        Queue<RBTreeNode> leaves = new LinkedList<RBTreeNode>();
        queue.add(this.root);
        while (!queue.isEmpty()) {
            RBTreeNode tempNode = queue.poll();
            if (tempNode.getLeft() != null) {
                queue.add(tempNode.getLeft());
            }
            if (tempNode.getRight() != null) {
                queue.add(tempNode.getRight());
            }
            if (!tempNode.hasChildren()) {
                leaves.add(tempNode);
            }
        }
        System.out.println("Pocet listov RB stromu je " + leaves.size());
        return leaves;
    }

    /**
     *
     */
    public void levelOrderRoot() {
        levelOrder(root);
    }

    /**
     *
     * @return
     */
    public List inOrderRoot() {
        return inOrder(root);
    }

    /**
     *
     * @param item
     * @return
     */
    public List inOrder(RBTreeNode item) {
        if (item == null) {
            System.out.println("Strom nenajdeny / root je null");
            return null;
        }
        Stack<RBTreeNode> stack = new Stack<>();
        RBTreeNode node = item;
        List<Object> inOrderNodes = new ArrayList<>();

        while (!stack.empty() || node != null) {

            // if it is not null, push to stack
            //and go down the tree to left
            if (node != null) {
                stack.push(node);
                node = node.getLeft();

                // if no left child
                // pop stack, process the node
                // then let p point to the right
            } else {
                RBTreeNode t = stack.pop();
                System.out.println("InOrder kluc je: " + t.getKey());
                inOrderNodes.add(t.getValue());
                node = t.getRight();
            }
        }
        return inOrderNodes;
    }
    
    /**
     *
     * @return
     */
    public List inOrderNodeRoot() {
        return inOrderNode(root);
    }

    /**
     *
     * @param item
     * @return
     */
    public List inOrderNode(RBTreeNode item) {
        if (item == null) {
            System.out.println("Strom nenajdeny / root je null");
            return null;
        }
        Stack<RBTreeNode> stack = new Stack<>();
        RBTreeNode node = item;
        List<RBTreeNode> inOrderNodes = new ArrayList<>();

        while (!stack.empty() || node != null) {

            // if it is not null, push to stack
            //and go down the tree to left
            if (node != null) {
                stack.push(node);
                node = node.getLeft();

                // if no left child
                // pop stack, process the node
                // then let p point to the right
            } else {
                RBTreeNode t = stack.pop();
                System.out.println("InOrder kluc je: " + t.getKey());
                inOrderNodes.add(t);
                node = t.getRight();
            }
        }
        return inOrderNodes;
    }
    
    public List postOrderRoot(){
        return postOrder(root);
    }
    
    public List postOrder(RBTreeNode node){
        if (node == null) {
            System.out.println("Strom nenajdeny / root je null");
            return null;
        }
        List<Object> postOrderList = new ArrayList<>();
        Stack<RBTreeNode> stack = new Stack<>();
        RBTreeNode last = null;
        
        while(!stack.isEmpty() || node != null){
            if (node!=null){
                stack.push(node);
                node=node.getLeft();
            }
            else {
                RBTreeNode peekNode = stack.peek();
                if (peekNode.getRight()!= null && last!=peekNode.getRight())
                    node=peekNode.getRight();
                else {
                    postOrderList.add(peekNode.getValue());
                    last = stack.pop();
                }
            }
        }
        return postOrderList;
    }

    /**
     *
     */
    public void colorTest() {
        Queue<RBTreeNode> leaves = this.searchLeaves();
        if (root != null) {
            this.getRoot().keyAndColorToString();

            while (!leaves.isEmpty()) {
                RBTreeNode tempNode = leaves.poll();
                int countBlack = 0;
                int count2Reds = 0;
                System.out.print("Pocet ciernych z listu " + tempNode.getKey() + ": ");
                while (tempNode != this.getRoot()) {
                    if (tempNode.isBlack()) {
                        countBlack++;
                    }
                    if (!tempNode.isBlack() && !tempNode.getParent().isBlack()) {
                        count2Reds++;
                    }
                    tempNode = tempNode.getParent();
                }
                System.out.println(countBlack + 1 + ", pocet dvojitych cervenych z listu: " + count2Reds);
            }
        }
    }

    /**
     *
     * @param key
     */
    public void delete(T key) {
        RBTreeNode delNode = find(key); //najdi node s klucom key
        if (delNode == null) {
            return;
        }
        deleteNode(delNode); //return, ak strom neobsahuje prvok s takymto klucom
    }

    /**
     *
     * @param delNode
     */
    public void deleteNode(RBTreeNode delNode) {
        if (delNode.hasBothChildren()) { //ak ma oboch synov
            deleteIfHasBothChildren(delNode);
        } else if (delNode.hasChildren() && !delNode.hasBothChildren()) { //ma deti, ale nie obe - prave 1 syn
            deleteIfHasOneChild(delNode);
        } else if (!delNode.hasChildren()) { //ak nema synov == je listom stromu
            deleteIfHasNoChildren(delNode);
        }
    }

    /**
     *
     * @param delNode
     */
    public void finalDelete(RBTreeNode delNode) {
        if (delNode != root) {
            if (delNode.isLeftChild()) {
                delNode.getParent().setLeft(null);
            } else if (delNode.isRightChild()) {
                delNode.getParent().setRight(null);
            }
        } else if (delNode == root) {
            root = null;
        }
        delNode.setParent(null);
        delNode.setLeft(null);
        delNode.setRight(null);
        delNode = null;
    }

    /**
     *
     * @param delNode
     */
    public void deleteIfHasBothChildren(RBTreeNode delNode) {
        RBTreeNode minValueNode = null;
        RBTreeNode minChild = null;
        RBTreeNode minParent = null;
        RBTreeNode delParent = null;
        RBTreeNode delR = null;
        RBTreeNode delL = null;
        boolean tempColor = false;

        minValueNode = minValueLeft(delNode.getRight());
        minChild = minValueNode.getRight();
        tempColor = minValueNode.isBlack();

        minParent = minValueNode.getParent();
        delParent = delNode.getParent();
        delR = delNode.getRight();
        delL = delNode.getLeft();

        if (minValueNode.getParent() == delNode) {
            minValueNode.setParent(delNode.getParent());
            if (delNode == root) {
                root = minValueNode;
            } else if (delNode != root) {
                if (delNode.isLeftChild()) {
                    delNode.getParent().setLeft(minValueNode);
                } else if (delNode.isRightChild()) {
                    delNode.getParent().setRight(minValueNode);
                }
            }
            delNode.setParent(minValueNode);
            minValueNode.setRight(delNode);
            minValueNode.setLeft(delNode.getLeft());
            if (minValueNode.getLeft() != null) {
                minValueNode.getLeft().setParent(minValueNode);
            }
            minValueNode.setBlack(delNode.isBlack());
            delNode.setBlack(tempColor);
            delNode.setLeft(null);
            delNode.setRight(minChild);
            if (minChild != null) {
                minChild.setParent(delNode);
            }
        } else {
            minValueNode.setParent(delParent);
            if (delNode == root) {
                root = minValueNode;
            } else if (delNode != root) {
                if (delNode.isLeftChild()) {
                    delParent.setLeft(minValueNode);
                } else if (delNode.isRightChild()) {
                    delParent.setRight(minValueNode);
                }
            }
            minValueNode.setLeft(delL);
            minValueNode.setBlack(delNode.isBlack());
            delNode.setBlack(tempColor);
            minValueNode.setRight(delR);
            minValueNode.getRight().setParent(minValueNode);
            minValueNode.getLeft().setParent(minValueNode);

            delNode.setParent(minParent);
            delNode.getParent().setLeft(delNode);
            delNode.setLeft(null);
            delNode.setRight(minChild);
            if (minChild != null) {
                minChild.setParent(delNode);
            }
        }
        deleteNode(delNode);
    }

    /**
     *
     * @param delNode
     */
    public void deleteIfHasOneChild(RBTreeNode delNode) {
        RBTreeNode tempDel = null;
        RBTreeNode childRight = null;
        RBTreeNode childLeft = null;
        boolean tempColor = false;
        if (delNode.getLeft() == null) { //ak ma praveho syna, nahradi ho pravy
            tempDel = delNode;
            childRight = delNode.getRight().getRight();
            childLeft = delNode.getRight().getLeft();
            tempColor = delNode.getRight().isBlack();

            if (delNode.isLeftChild()) {
                delNode.getParent().setLeft(delNode.getRight());
            } else if (delNode.isRightChild()) {
                delNode.getParent().setRight(delNode.getRight());
            }

            delNode.getRight().setParent(delNode.getParent());
            if (delNode.getParent() == null) {
                root = delNode.getRight();
            }
            delNode.getRight().setLeft(null);
            delNode.getRight().setBlack(delNode.isBlack());
            delNode.setBlack(tempColor);
            delNode.getRight().setRight(delNode);

            delNode.setParent(tempDel.getRight());

            delNode.setLeft(childLeft);
            if (childLeft != null) {
                delNode.getLeft().setParent(delNode);
            }
            delNode.setRight(childRight);
            if (childRight != null) {
                delNode.getRight().setParent(delNode);
            }

            deleteNode(delNode);

        } else if (delNode.getRight() == null) { //ak ma laveho syna, ten ho nahradi
            tempDel = delNode;
            childRight = delNode.getLeft().getRight();
            childLeft = delNode.getLeft().getLeft();
            tempColor = delNode.getLeft().isBlack();

            if (delNode.isLeftChild()) {
                delNode.getParent().setLeft(delNode.getLeft());
            } else if (delNode.isRightChild()) {
                delNode.getParent().setRight(delNode.getLeft());
            }

            delNode.getLeft().setParent(delNode.getParent());
            if (delNode.getParent() == null) {
                root = delNode.getLeft();
            }
            delNode.getLeft().setRight(null);
            delNode.getLeft().setBlack(delNode.isBlack());
            delNode.setBlack(tempColor);
            delNode.getLeft().setLeft(delNode);

            delNode.setParent(tempDel.getLeft());

            delNode.setLeft(childLeft);
            if (childLeft != null) {
                delNode.getLeft().setParent(delNode);
            }
            delNode.setRight(childRight);
            if (childRight != null) {
                delNode.getRight().setParent(delNode);
            }

            deleteNode(delNode);
        }
    }

    /**
     *
     * @param delNode
     */
    public void deleteIfHasNoChildren(RBTreeNode delNode) {
        RBTreeNode brother = delNode.getBrother();
        RBTreeNode parent = delNode.getParent();
        boolean tempColor = delNode.isBlack();
        delNode.setBlack(false);
        finalDelete(delNode);
        if (brother!= null && tempColor) {
            fixAfterDelete(brother);
        } else if (brother==null && tempColor){
            if (parent != root) {
            if (parent.isBlack()) {
                fixAfterDelete(parent.getBrother());
            } else {
                parent.setBlack(true);
            }
        }
    }
    }

    /**
     *
     * @param i
     */
    public void fixAfterDelete(RBTreeNode i) {//i je brat mazaneho prvku
        if (i != null && i.isBlack()) {
            if (((i.getLeft() != null && i.getLeft().isBlack()) || i.getLeft() == null)
                    && ((i.getRight() != null && i.getRight().isBlack()) || i.getRight() == null)) {
// case 1: brat je cierny a obaja synovia su tiez cierni
                fixDeleteCase1(i);
            } else if (i.getLeft() != null && !i.getLeft().isBlack() && i.isLeftChild()) {
// case 2: mazany je pravy, brat je cierny a jeho lavy syn je cerveny  
                    fixDeleteCase2Right(i);
            } else if (i.getRight() != null && !i.getRight().isBlack() && i.isRightChild()) {
// case 2: mazany je lavy, brat je cierny a jeho pravy syn je cerveny 
                    fixDeleteCase2Left(i);
            } else if (i.getLeft() != null && !i.getLeft().isBlack() && (i.getRight() == null || i.getRight().isBlack()) && i.isRightChild()) {
// case 3: mazany je lavy, brat a pravy syn su cierni, lavy syn je cerveny
                i.setBlack(false);
                i.getLeft().setBlack(true);
                rotateRight(i, true);
                fixDeleteCase2Left(i.getParent());
            } else if (i.getRight() != null && !i.getRight().isBlack() && (i.getLeft() == null || i.getLeft().isBlack()) && i.isLeftChild()) {
// case 3: brat a lavy syn su cierni, pravy syn je cerveny
                i.setBlack(false);
                i.getRight().setBlack(true);
                rotateLeft(i, true);
                fixDeleteCase2Right(i.getParent());
            }
        } else if (i != null && !i.isBlack()) {
// case 4: brat je cerveny
            fixDeleteCase4(i);
        }
    }

    /**
     *
     * @param i
     */
    public void fixDeleteCase1(RBTreeNode i) {
        i.setBlack(false);
        if (i.getParent() != root && i != root) {
            if (i.getParent().isBlack()) {
                fixAfterDelete(i.getParent().getBrother());
            } else {
                i.getParent().setBlack(true);
            }
        }
    }

    /**
     *
     * @param i
     */
    public void fixDeleteCase2Left(RBTreeNode i) {
        i.getRight().setBlack(true);
        i.setBlack(i.getParent().isBlack());
        i.getParent().setBlack(true);
        rotateLeft(i.getParent(), true);
    }

    /**
     *
     * @param i
     */
    public void fixDeleteCase2Right(RBTreeNode i) {
        i.getLeft().setBlack(true);
        i.setBlack(i.getParent().isBlack());
        i.getParent().setBlack(true);
        rotateRight(i.getParent(), true);
    }

    /**
     *
     * @param i
     */
    public void fixDeleteCase4(RBTreeNode i) {
        RBTreeNode sibling = null;
        RBTreeNode parent=i.getParent();
        i.getParent().setBlack(false);
        i.setBlack(true);
        if (i.isLeftChild()) {
            rotateRight(i.getParent(), true);
            sibling=i.getRight().getLeft();
        } else if (i.isRightChild()) {
            rotateLeft(i.getParent(), true);
            sibling=i.getLeft().getRight();
        }
        //if (parent.getRight()!=null)fixAfterDelete(parent.getRight());
        //else if (parent.getLeft()!=null)fixAfterDelete(parent.getLeft());
        if (sibling !=null)fixAfterDelete(sibling);
    }

    /**
     *
     * @param n
     * @return
     */
    public RBTreeNode minValueLeft(RBTreeNode n) {
        while (n.getLeft() != null) {
            n = n.getLeft();
        }
        return n;
    }

    /**
     *
     * @param n
     * @return
     */
    public RBTreeNode minValueRight(RBTreeNode n) {
        while (n.getRight() != null) {
            n = n.getRight();
        }
        return n;
    }

    /**
     *
     * @return
     */
    public RBTreeNode getRoot() {
        return root;
    }

    /**
     *
     * @param root
     */
    public void setRoot(RBTreeNode root) {
        this.root = root;
    }
}

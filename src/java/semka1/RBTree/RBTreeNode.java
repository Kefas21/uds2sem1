/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semka1.RBTree;

/**
 *
 * @author peter Vytvorenie listu RB stromu, nastavenie referencii na rodica,
 * laveho a praveho syna
 * @param <T>
 * @param <V>
 */
public class RBTreeNode<T extends Comparable<T>, V> {

    private T key;
    private V value;

    private RBTreeNode parent;
    private RBTreeNode left;
    private RBTreeNode right;
    private boolean black;

    /**
     *
     */
    public RBTreeNode() {
        black = false;
        parent = null;
        left = null;
        right = null;
    }

    /**
     *
     * @param key
     */
    public RBTreeNode(T key) {
        this();
        this.key = key;
        this.black = false;
    }

    /**
     *
     * @param key
     * @param value
     */
    public RBTreeNode(T key, V value) {
        this();
        this.key = key;
        this.value = value;
        this.black = false;
    }

    /**
     *
     */
    public void keyToString() {
        System.out.println("Kľúč listu je: " + this.key);
    }

    /**
     *
     */
    public void keyAndColorToString() {
        System.out.println("Kľúč prvku je: " + this.key + " a je cierny: " + this.black);
    }
    
    /**
     *
     */
    public void allInfoToString(){
        System.out.println("Kluc je "+this.key+", otec: "+this.parent+", lavy: "+this.left+", pravy: "+this.right+" a je cierny: "+this.black);
    }

    /**
     *
     * @return
     */
    public boolean hasBrother() {
        if (this.getParent() != null) {
            if (this.isRightChild() && this.getParent().getLeft() != null) {
                return true;
            } else if (this.isLeftChild() && this.getParent().getRight() != null) {
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @return
     */
    public RBTreeNode getBrother() {
        if (this.parent != null) {
            if (this.isRightChild()) {
                return this.parent.left;
            } else if (this.isLeftChild()) {
                return this.parent.right;
            }
        }
        return null;
    }

    /**
     *
     * @return
     */
    public boolean hasChildren() {
        return (this.left != null || this.right != null);
    }

    /**
     *
     * @return
     */
    public boolean hasBothChildren() {
        return (this.left != null && this.right != null);
    }

    /**
     *
     * @return
     */
    public boolean isChild() {
        return parent != null;
    }

    /**
     *
     * @param p
     * @return
     */
    public boolean isChild(RBTreeNode p) {
        return p == parent;
    }

    /**
     *
     * @return
     */
    public boolean isRightChild() {
        if (this.parent!=null){
        if (this.parent.right!=null) return this.parent.right == this;
        }
        return false;
    }

    /**
     *
     * @return
     */
    public boolean isLeftChild() {
        if (this.parent!=null){
        if (this.parent.left!=null) return this.parent.left == this;
        }
        return false;
    }

    /**
     *
     * @return
     */
    public T getKey() {
        return key;
    }

    /**
     *
     * @param key
     */
    public void setKey(T key) {
        this.key = key;
    }

    /**
     *
     * @return
     */
    public V getValue() {
        return value;
    }

    /**
     *
     * @param value
     */
    public void setValue(V value) {
        this.value = value;
    }

    /**
     *
     * @return
     */
    public RBTreeNode getParent() {
        return parent;
    }

    /**
     *
     * @param parent
     */
    public void setParent(RBTreeNode parent) {
        this.parent = null;
        this.parent = parent;
    }

    /**
     *
     * @return
     */
    public RBTreeNode getLeft() {
        return left;
    }

    /**
     *
     * @param left
     */
    public void setLeft(RBTreeNode left) {
        this.left = null;
        this.left = left;
    }

    /**
     *
     * @return
     */
    public RBTreeNode getRight() {
        return right;
    }

    /**
     *
     * @param right
     */
    public void setRight(RBTreeNode right) {
        this.right = null;
        this.right = right;
    }

    /**
     *
     * @return
     */
    public boolean isBlack() {
        return black;
    }

    /**
     *
     * @param black
     */
    public void setBlack(boolean black) {
        this.black = black;
    }

}

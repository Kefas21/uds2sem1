/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semka1.beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import semka1.RBTree.RBTreeNode;
import semka1.entities.Bill;
import semka1.entities.Hospital;
import semka1.entities.HospitalIS;
import semka1.entities.Hospitalization;
import semka1.entities.InsuranceCompany;
import semka1.entities.Patient;
import semka1.entities.StartEndID;

/**
 *
 * @author peter
 */
@ManagedBean(name = "hospISBean", eager = true)
@SessionScoped
public class HospitalISBean {

    private HospitalIS hospIS;
    private Hospital hospital;
    private String info;
    private Patient patient;
    private Hospitalization hosp;
    private InsuranceCompany ins;
    private List<Object> patientList;
    private List<Object> hospitalList;
    private List<Object> hospList;
    private List<Object> insList;
    private List<Integer> counterList;
    private List<Bill> billList;
    private int hospNumber;
    private int patNumber;
    private int insNumber = 3;
    private int hospitNumber;
    private Date date;
    private String name;
    private int month;
    private int year;

    /**
     *
     */
    public HospitalISBean() {
        this.info = "";
        this.hospIS = new HospitalIS();
        this.hospital = new Hospital();
        this.ins = new InsuranceCompany();
        this.patientList = new ArrayList<>();
        this.hospitalList = new ArrayList<>();
        this.hospList = new ArrayList<>();
        this.insList = new ArrayList<>();
        this.counterList = new ArrayList<>();
        this.billList = new ArrayList<>();
        this.patient = new Patient();
        this.hosp = new Hospitalization();
        this.date = new Date();

    }

    /**
     *
     */
    public void addHospital() {
        this.info = "";
        info = "Hospital " + hospital.getName() + ": ";
        info += hospIS.addHospital(new Hospital(hospital.getName()), insNumber);
        setNewAtt();
    }

    /**
     *
     */
    public void addPatient() {
        this.info = "";
        info = "Patient " + patient.getIDnumber() + ": ";
        info += hospIS.addPatient(new Patient(patient.getFirstName(), patient.getSurname(), patient.getIDnumber(),
                patient.getBirth(), patient.getIDinsurance()));
        //hospIS.getPatientTree().levelOrderRoot();
        setNewAtt();
    }

    /**
     *
     */
    public void addHospitalizationStart() {
        this.info = "";
        if (hosp.getEnd() != null && hosp.getStart().compareTo(hosp.getEnd()) > 0) {
            this.info = "End date is bigger than start date";
            return;
        }
        if (hosp.getEnd() == null) {
            hosp.setEnd(new Date(600, 0, 1));
        }
        info = "Hospitalization for patient" + hosp.getPatientID() + ": ";
        info += hospIS.addHospitalizationStart(hosp);
        setNewAtt();
    }

    /**
     *
     */
    public void addHospitalizationEnd() {
        this.info = "";
        if (hosp.getStart().compareTo(hosp.getEnd()) > 0) {
            this.info = "Start date is bigger than end date";
            return;
        }
        Date endDate = new Date(600, 0, 1);
        info = "Hospitalization for patient" + patient.getIDnumber() + ": ";
        info += hospIS.addHospitalizationEnd(hosp, endDate);
        setNewAtt();
    }
    
    public void deleteHospital(){
        info=hospIS.deleteHospital(name,hospital.getName());
    }

    /**
     *
     */
    public void showActualHospitPatients() {
        hospList = hospIS.showActualHospitPatients(hospital.getName(), date);
        setNewAtt();
    }

    /**
     *
     */
    public void showActualHospitPatientsByInsurance() {
        hospList = hospIS.showActualHospitPatientsByInsurance(hospital.getName(), date, ins.getInsID());
        setNewAtt();
    }

    /**
     *
     */
    public void showActualHospitPatientsSorted() {
        hospList = hospIS.showActualHospitPatientsSorted(hospital.getName(), date, ins.getInsID());
        setNewAtt();
    }

    /**
     *
     */
    public void showPatientsRange() {
        hospList = hospIS.showPatientsRange(hospital.getName(), hosp.getStart(), hosp.getEnd());
        setNewAtt();
    }

    /**
     *
     * @return
     */
    public String fillHospitalSortedByName() {
        hospitalList = hospIS.fillHospitalsSortedByName();
        return "12showHospitals?faces-redirect=true";
    }
    
    /**
     *
     * @return
     */
    public String fillHospitalPostOrder() {
        hospitalList = hospIS.fillHospitalsPostOrder();
        return "14showHospitals?faces-redirect=true";
    }

    /**
     *
     */
    public void findPatientByID() {
        patientList = hospIS.findPatientByID(hospital.getName(), patient.getIDnumber());
        setNewAtt();
    }

    /**
     *
     */
    public void findPatientByName() {
        patientList = hospIS.findPatientByName(hospital.getName(), patient.getFirstName(), patient.getSurname());
        setNewAtt();
    }

    /**
     *
     */
    public void bills() {
        Date start = new Date(year - 1900, month - 1, 1);
        Date end = new Date(year - 1900, month, 0);
        //Calendar start = new GregorianCalendar(year, month-1, 1);
        //Calendar end = new GregorianCalendar(year, month-1, start.getActualMaximum(Calendar.DAY_OF_MONTH));        
        billList = hospIS.bills(start, end, insNumber);
        counterList = hospIS.getCounts();
        setNewAtt();
    }

    /**
     *
     */
    public void generateIS() {
        this.info = "";
        int i = 0;
        int j = 0;
        int m = 0;
        int n = 0;
        int rc = 999999999;
        String name = "Name";
        String surname = "Surname";
        String hospName = "Hospital";
        Date birth = new Date(70, 0, 1);
        Random rn = new Random();
        int year;
        int month;
        int day;
        String patID;

        for (j = 1; j <= insNumber; j++) {
            ins = new InsuranceCompany(j);
            hospIS.getInsuranceTree().insert(new RBTreeNode(ins.getInsID(), ins));
        }

        for (i = 0; i < hospNumber; i++) {
            hospital = new Hospital((String) hospName + i);
            for (j = 1; j <= insNumber; j++) {
                ins = new InsuranceCompany(j);
                hospital.getInsuranceTree().insert(new RBTreeNode<>(ins.getInsID(), ins));
                for (m = 0; m < patNumber; m++) {
                    rc++;
                    patID = Integer.toString(rc);
                    patient = new Patient(name + m, surname + m, patID, birth, ins.getInsID());
                    String key = patient.getFirstName().concat(patient.getSurname()).concat(patient.getIDnumber());

                    for (n = 0; n < hospitNumber; n++) {
                        year = rn.nextInt(116 - 71 + 1) + 71;
                        month = rn.nextInt(11 - 0 + 1) + 0;
                        day = rn.nextInt(28 - 1 + 1) + 1;
                        Date start = new Date(year, month, day);
                        Date end = new Date(year + 2, month, day);
                        hosp = new Hospitalization(start, end, "pro coder", patID, hospital.getName());
                        StartEndID dateKey = new StartEndID(start, end, patID);
                        RBTreeNode ic = hospIS.getInsuranceTree().find(patient.getIDinsurance());
                        InsuranceCompany icVal = (InsuranceCompany) ic.getValue();
                        icVal.getHospitalizationTree().insert(new RBTreeNode(dateKey, hosp));
                        hospital.getHospitTree().insert(new RBTreeNode(dateKey, hosp));

                        RBTreeNode hs = hospital.getInsuranceTree().find(patient.getIDinsurance());
                        InsuranceCompany hsic = (InsuranceCompany) hs.getValue();
                        hsic.getHospitalizationTree().insert(new RBTreeNode(dateKey, hosp));
                    }
                    hospIS.getPatientTree().insert(new RBTreeNode(patient.getIDnumber(), patient));
                    hospital.getPatientTree1().insert(new RBTreeNode<>(patient.getIDnumber(), patient));
                    hospital.getPatientTree2().insert(new RBTreeNode<>(key, patient));
                }
            }
            hospIS.addHospital(hospital);
            //hospIS.getHospitalTree().levelOrderRoot();
        }
        info = hospNumber + " hospitals were generated, "
                + patNumber + " patients were generated in every hospital, "
                + hospitNumber + " hospitalizations were generated for every patient";
        setNewAtt();
    }

    /**
     *
     */
    public void save() {
        info = hospIS.save();
    }

    /**
     *
     */
    public void load() {
        info = hospIS.load();
    }

    /**
     *
     */
    public void resetInfoString() {
        this.info = "";
    }

    /**
     *
     * @return
     */
    public HospitalIS getHospIS() {
        return hospIS;
    }

    /**
     *
     */
    public void setNewAtt() {
        hospital = new Hospital();
        patient = new Patient();
        ins = new InsuranceCompany();
        hosp = new Hospitalization();
        date = new Date();
    }

    /**
     *
     * @param hospIS
     */
    public void setHospIS(HospitalIS hospIS) {
        this.hospIS = hospIS;
    }

    /**
     *
     * @return
     */
    public Hospital getHospital() {
        return hospital;
    }

    /**
     *
     * @param hospital
     */
    public void setHospital(Hospital hospital) {
        this.hospital = hospital;
    }

    /**
     *
     * @return
     */
    public String getInfo() {
        return info;
    }

    /**
     *
     * @param info
     */
    public void setInfo(String info) {
        this.info = info;
    }

    /**
     *
     * @return
     */
    public Patient getPatient() {
        return patient;
    }

    /**
     *
     * @param patient
     */
    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    /**
     *
     * @return
     */
    public List<Object> getPatientList() {
        return patientList;
    }

    /**
     *
     * @param patientList
     */
    public void setPatientList(List<Object> patientList) {
        this.patientList = patientList;
    }

    /**
     *
     * @return
     */
    public List<Object> getHospitalList() {
        return hospitalList;
    }

    /**
     *
     * @param hospitalList
     */
    public void setHospitalList(List<Object> hospitalList) {
        this.hospitalList = hospitalList;
    }

    /**
     *
     * @return
     */
    public List<Object> getHospList() {
        return hospList;
    }

    /**
     *
     * @param hospList
     */
    public void setHospList(List<Object> hospList) {
        this.hospList = hospList;
    }

    /**
     *
     * @return
     */
    public int getHospNumber() {
        return hospNumber;
    }

    /**
     *
     * @param hospNumber
     */
    public void setHospNumber(int hospNumber) {
        this.hospNumber = hospNumber;
    }

    /**
     *
     * @return
     */
    public int getPatNumber() {
        return patNumber;
    }

    /**
     *
     * @param patNumber
     */
    public void setPatNumber(int patNumber) {
        this.patNumber = patNumber;
    }

    /**
     *
     * @return
     */
    public int getInsNumber() {
        return insNumber;
    }

    /**
     *
     * @param insNumber
     */
    public void setInsNumber(int insNumber) {
        this.insNumber = insNumber;
    }

    /**
     *
     * @return
     */
    public Hospitalization getHosp() {
        return hosp;
    }

    /**
     *
     * @param hosp
     */
    public void setHosp(Hospitalization hosp) {
        this.hosp = hosp;
    }

    /**
     *
     * @return
     */
    public InsuranceCompany getIns() {
        return ins;
    }

    /**
     *
     * @param ins
     */
    public void setIns(InsuranceCompany ins) {
        this.ins = ins;
    }

    /**
     *
     * @return
     */
    public List<Object> getInsList() {
        return insList;
    }

    /**
     *
     * @param insList
     */
    public void setInsList(List<Object> insList) {
        this.insList = insList;
    }

    /**
     *
     * @return
     */
    public int getHospitNumber() {
        return hospitNumber;
    }

    /**
     *
     * @param hospitNumber
     */
    public void setHospitNumber(int hospitNumber) {
        this.hospitNumber = hospitNumber;
    }

    /**
     *
     * @return
     */
    public Date getDate() {
        return date;
    }

    /**
     *
     * @param date
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     */
    public int getMonth() {
        return month;
    }

    /**
     *
     * @param month
     */
    public void setMonth(int month) {
        this.month = month;
    }

    /**
     *
     * @return
     */
    public int getYear() {
        return year;
    }

    /**
     *
     * @param year
     */
    public void setYear(int year) {
        this.year = year;
    }

    /**
     *
     * @return
     */
    public List<Integer> getCounterList() {
        return counterList;
    }

    /**
     *
     * @param counterList
     */
    public void setCounterList(List<Integer> counterList) {
        this.counterList = counterList;
    }

    /**
     *
     * @return
     */
    public List<Bill> getBillList() {
        return billList;
    }

    /**
     *
     * @param billList
     */
    public void setBillList(List<Bill> billList) {
        this.billList = billList;
    }

}

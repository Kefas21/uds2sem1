/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semka1.entities;

import java.util.Date;

/**
 *
 * @author peter
 */
public class Bill {
    private Date date;
    private String patientID;
    private String hospital;
    private String diagnosis;

    /**
     *
     * @param date
     * @param patientID
     * @param hospital
     * @param diagnosis
     */
    public Bill(Date date, String patientID, String hospital, String diagnosis) {
        this.date = date;
        this.patientID = patientID;
        this.hospital = hospital;
        this.diagnosis = diagnosis;
    }

    /**
     *
     * @return
     */
    public Date getDate() {
        return date;
    }

    /**
     *
     * @param date
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     *
     * @return
     */
    public String getPatientID() {
        return patientID;
    }

    /**
     *
     * @param patientID
     */
    public void setPatientID(String patientID) {
        this.patientID = patientID;
    }

    /**
     *
     * @return
     */
    public String getHospital() {
        return hospital;
    }

    /**
     *
     * @param hospital
     */
    public void setHospital(String hospital) {
        this.hospital = hospital;
    }

    /**
     *
     * @return
     */
    public String getDiagnosis() {
        return diagnosis;
    }

    /**
     *
     * @param diagnosis
     */
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }
    
    
}

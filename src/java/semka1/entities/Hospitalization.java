/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semka1.entities;

import java.util.Date;

/**
 *
 * @author peter
 */
public class Hospitalization {
    
    private Date start;
    private Date end;
    private String diagnosis;
    private String patientID;
    private String hospitalName;
    
    /**
     *
     */
    public Hospitalization(){}

    /**
     *
     * @param start
     * @param end
     * @param diagnosis
     * @param patientID
     * @param hospitalName
     */
    public Hospitalization(Date start, Date end, String diagnosis, String patientID, String hospitalName) {
        this.start = start;
        this.end = end;
        this.diagnosis = diagnosis;
        this.patientID = patientID;
        this.hospitalName = hospitalName;
    }
    
    /**
     *
     * @param start
     * @param end
     * @param injury
     */
    public Hospitalization(Date start, Date end, String injury) {
        this.start = start;
        this.end = end;
        this.diagnosis=injury;
    }
    
    /**
     *
     * @param start
     * @param injury
     */
    public Hospitalization(Date start, String injury) {
        this.start = start;
        this.end = null;
        this.diagnosis=injury;
    }

    /**
     *
     * @return
     */
    public Date getStart() {
        return start;
    }

    /**
     *
     * @param start
     */
    public void setStart(Date start) {
        this.start = start;
    }

    /**
     *
     * @return
     */
    public Date getEnd() {
        return end;
    }

    /**
     *
     * @param end
     */
    public void setEnd(Date end) {
        this.end = end;
    }   

    /**
     *
     * @return
     */
    public String getDiagnosis() {
        return diagnosis;
    }

    /**
     *
     * @param diagnosis
     */
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    /**
     *
     * @return
     */
    public String getPatientID() {
        return patientID;
    }

    /**
     *
     * @param patientID
     */
    public void setPatientID(String patientID) {
        this.patientID = patientID;
    }

    /**
     *
     * @return
     */
    public String getHospitalName() {
        return hospitalName;
    }

    /**
     *
     * @param hospitalName
     */
    public void setHospitalName(String hospitalName) {
        this.hospitalName = hospitalName;
    }
    
}

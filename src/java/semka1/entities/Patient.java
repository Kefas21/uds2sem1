/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semka1.entities;

import java.util.Date;

/**
 *
 * @author peter
 */
public class Patient {
    private String firstName;
    private String surname;
    private String IDnumber;
    private Date birth;
    private int IDinsurance;

    /**
     *
     */
    public Patient(){}
    
    /**
     *
     * @param firstName
     * @param surname
     * @param IDnumber
     * @param birth
     * @param IDinsurance
     */
    public Patient(String firstName, String surname, String IDnumber, Date birth, int IDinsurance) {
        this.firstName = firstName;
        this.surname = surname;
        this.IDnumber = IDnumber;
        this.birth = birth;
        this.IDinsurance = IDinsurance;
        //this.hospitalizationTree1=new RBTree<>();
    }

    /**
     *
     * @return
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     *
     * @param firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     *
     * @return
     */
    public String getSurname() {
        return surname;
    }

    /**
     *
     * @param surname
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     *
     * @return
     */
    public String getIDnumber() {
        return IDnumber;
    }

    /**
     *
     * @param IDnumber
     */
    public void setIDnumber(String IDnumber) {
        this.IDnumber = IDnumber;
    }

    /**
     *
     * @return
     */
    public Date getBirth() {
        return birth;
    }

    /**
     *
     * @param birth
     */
    public void setBirth(Date birth) {
        this.birth = birth;
    }

    /**
     *
     * @return
     */
    public int getIDinsurance() {
        return IDinsurance;
    }

    /**
     *
     * @param IDinsurance
     */
    public void setIDinsurance(int IDinsurance) {
        this.IDinsurance = IDinsurance;
    } 
}

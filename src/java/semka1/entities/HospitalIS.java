/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semka1.entities;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import semka1.RBTree.RBTree;
import semka1.RBTree.RBTreeNode;
import semka1.csv.CsvReader;
import semka1.csv.CsvWriter;

/**
 *
 * @author peter
 */
public class HospitalIS {

    private RBTree<String> hospitalTree = new RBTree<>();
    private RBTree<String> patientTree = new RBTree<>();
    private RBTree<Integer> insuranceTree = new RBTree<>();
    List<Integer> counts = new ArrayList<>();

    /**
     *
     */
    public HospitalIS() {
    }

    /**
     *
     * @param h
     * @return
     */
    public String addHospital(Hospital h) {
        RBTreeNode hospital = new RBTreeNode<>(h.getName(), h);
        boolean control = hospitalTree.insert(hospital);
        //hospitalTree.levelOrderRoot();
        if (control) {
            return "Hospital was added!";
        } else {
            return "Hospital with this name is already in tree!";
        }
    }

    /**
     *
     * @param h
     * @param insNumber
     * @return
     */
    public String addHospital(Hospital h, int insNumber) {
        int j = 0;
        for (j = 1; j <= insNumber; j++) {
            InsuranceCompany ic = new InsuranceCompany(j);
            h.getInsuranceTree().insert(new RBTreeNode<>(ic.getInsID(), ic));
        }
        RBTreeNode hospital = new RBTreeNode<>(h.getName(), h);
        boolean control = hospitalTree.insert(hospital);
        //hospitalTree.levelOrderRoot();
        if (control) {
            return "Hospital was added!";
        } else {
            return "Hospital with this name is already in tree!";
        }
    }

    /**
     *
     * @param p
     * @return
     */
    public String addPatient(Patient p) {
        RBTreeNode newPat = new RBTreeNode<>(p.getIDnumber(), p);
        boolean control1 = patientTree.insert(newPat);
        if (control1) {
            return "Patient was added!";
        } else {
            return "Patient with this ID is already in tree!";
        }
    }

    /**
     *
     * @param h
     * @return
     */
    public String addHospitalizationStart(Hospitalization h) {
        StartEndID dat = new StartEndID(h.getStart(), h.getEnd(), h.getPatientID()); //vyskladanie StartEndID
        RBTreeNode hosp = new RBTreeNode(dat, h);

        RBTreeNode hos = hospitalTree.find(h.getHospitalName());
        if (hos == null) {
            return "Hospital not found";
        }
        Hospital hosValue = (Hospital) hos.getValue(); //najdem nemocnicu
        RBTreeNode pat = hosValue.getPatientTree1().find(h.getPatientID());
        if (pat == null) { //ak este pacient nie je v nemocnoci
            pat = patientTree.find(h.getPatientID());

            if (pat == null) {
                return "Patient not found. Please add new patient";
            }
            Patient patValue = (Patient) pat.getValue();
            String name = patValue.getFirstName();
            String surname = patValue.getSurname();
            String key = name.concat(surname).concat(h.getPatientID());
            hosValue.getPatientTree1().insert(new RBTreeNode(h.getPatientID(), patValue));
            hosValue.getPatientTree2().insert(new RBTreeNode(key, patValue));
            boolean control = hosValue.getHospitTree().insert(hosp);

            int ic = patValue.getIDinsurance();
            RBTreeNode ichos = insuranceTree.find(ic);
            InsuranceCompany ichosVal = (InsuranceCompany) ichos.getValue();
            ichosVal.getHospitalizationTree().insert(hosp);

            RBTreeNode icNode = hosValue.getInsuranceTree().find(ic);
            InsuranceCompany icValue = (InsuranceCompany) icNode.getValue();
            icValue.getHospitalizationTree().insert(hosp);

            return "Patient added!";
        } else {
            boolean control = hosValue.getHospitTree().insert(hosp);
            Patient patValue = (Patient) pat.getValue();

            int ic = patValue.getIDinsurance();
            RBTreeNode ichos = insuranceTree.find(ic);
            InsuranceCompany ichosVal = (InsuranceCompany) ichos.getValue();
            ichosVal.getHospitalizationTree().insert(hosp);

            RBTreeNode icNode = hosValue.getInsuranceTree().find(ic);
            InsuranceCompany icValue = (InsuranceCompany) icNode.getValue();
            icValue.getHospitalizationTree().insert(hosp);
            return "Hospitalization to patient added!";
        }
    }

    /**
     *
     * @param h
     * @param oldEnd
     * @return
     */
    public String addHospitalizationEnd(Hospitalization h, Date oldEnd) {
        StartEndID oldDate = new StartEndID(h.getStart(), oldEnd, h.getPatientID());
        StartEndID newDate = new StartEndID(h.getStart(), h.getEnd(), h.getPatientID());
        RBTreeNode hos = hospitalTree.find(h.getHospitalName());
        if (hos == null) {
            return "Hospital not found";
        }
        Hospital hosValue = (Hospital) hos.getValue(); //najdem nemocnicu
        RBTreeNode pat = hosValue.getPatientTree1().find(h.getPatientID());
        if (pat == null) { //ak este pacient nie je v nemocnoci
            return "Patient not found";
        } else {
            Patient patValue = (Patient) pat.getValue();
            RBTreeNode hospit = hosValue.getHospitTree().find(oldDate);
            if (hospit == null) {
                return "Hospitalization not found";
            }
            Hospitalization hValue = (Hospitalization) hospit.getValue();
            hValue.setEnd(h.getEnd());
            hosValue.getHospitTree().delete(oldDate);
            hosValue.getHospitTree().insert(new RBTreeNode(newDate, hValue));

            int ic = patValue.getIDinsurance();
            RBTreeNode icNode = hosValue.getInsuranceTree().find(ic);
            InsuranceCompany icValue = (InsuranceCompany) icNode.getValue();
            icValue.getHospitalizationTree().delete(oldDate);
            icValue.getHospitalizationTree().insert(new RBTreeNode(newDate, hValue));

            RBTreeNode ichNode = insuranceTree.find(ic);
            InsuranceCompany ichValue = (InsuranceCompany) ichNode.getValue();
            ichValue.getHospitalizationTree().delete(oldDate);
            ichValue.getHospitalizationTree().insert(new RBTreeNode(newDate, hValue));
            return "Hospitalization ended succesfully";
        }
    }

    /**
     *
     * @param hName
     * @param pID
     * @return
     */
    public List findPatientByID(String hName, String pID) {
        RBTreeNode hos = hospitalTree.find(hName);
        if (hos == null) {
            return null;
        }
        Hospital hosValue = (Hospital) hos.getValue();
        RBTreeNode pat = hosValue.getPatientTree1().find(pID);
        if (pat == null) {
            return null;
        }
        ArrayList<Patient> found = new ArrayList<Patient>();
        found.add((Patient) pat.getValue());
        return found;
    }

    /**
     *
     * @param hName
     * @param pName
     * @param pSurname
     * @return
     */
    public List findPatientByName(String hName, String pName, String pSurname) {
        String searchedKey = pName.concat(pSurname);
        RBTreeNode hos = hospitalTree.find(hName);
        if (hos == null) {
            return null;
        }
        Hospital hosValue = (Hospital) hos.getValue();
        ArrayList<Patient> found = (ArrayList<Patient>) hosValue.getPatientTree2().findMultipleNodes(searchedKey);
        return found;
    }

    /**
     *
     * @param hName
     * @param d
     * @return
     */
    public List showActualHospitPatients(String hName, Date d) {
        List<Hospitalization> actualPatients = new ArrayList<>();
        Queue<RBTreeNode> hospits = new LinkedList<>();
        RBTreeNode hos = hospitalTree.find(hName);
        if (hos == null) {
            return null;
        }
        Hospital hosValue = (Hospital) hos.getValue(); //najdem nemocnicu
        hospits.add(hosValue.getHospitTree().getRoot());
        while (!hospits.isEmpty()) {
            RBTreeNode currentHospit = hospits.poll();
            Hospitalization curHospVal = (Hospitalization) currentHospit.getValue();
            if (curHospVal.getStart().compareTo(d) <= 0 && curHospVal.getEnd().compareTo(d) >= 0) {
                actualPatients.add(curHospVal);
            }
            if (curHospVal.getStart().compareTo(d) <= 0 && d.compareTo(curHospVal.getEnd()) <= 0) {
                if (currentHospit.getLeft() != null) {
                    hospits.add(currentHospit.getLeft());
                }
                if (currentHospit.getRight() != null) {
                    hospits.add(currentHospit.getRight());
                }
            } else if (currentHospit.getRight() != null && curHospVal.getEnd().compareTo(d) < 0) {
                hospits.add(currentHospit.getRight());
            } else if (currentHospit.getLeft() != null && d.compareTo(curHospVal.getStart()) < 0) {
                hospits.add(currentHospit.getLeft());
            }
        }
        return actualPatients;
    }

    /**
     *
     * @param hName
     * @param d
     * @param ins
     * @return
     */
    public List showActualHospitPatientsByInsurance(String hName, Date d, int ins) {
        List<Hospitalization> actualPatients = new ArrayList<>();
        Queue<RBTreeNode> hospits = new LinkedList<>();
        RBTreeNode hos = hospitalTree.find(hName);
        if (hos == null) {
            return null;
        }
        Hospital hosValue = (Hospital) hos.getValue(); //najdem nemocnicu
        RBTreeNode ic = hosValue.getInsuranceTree().find(ins);
        InsuranceCompany icVal = (InsuranceCompany) ic.getValue();
        hospits.add(icVal.getHospitalizationTree().getRoot());
        while (!hospits.isEmpty()) {
            RBTreeNode currentHospit = hospits.poll();
            Hospitalization curHospVal = (Hospitalization) currentHospit.getValue();
            if (curHospVal.getStart().compareTo(d) <= 0 && curHospVal.getEnd().compareTo(d) >= 0) {
                actualPatients.add(curHospVal);
            }
            if (curHospVal.getStart().compareTo(d) <= 0 && d.compareTo(curHospVal.getEnd()) <= 0) {
                if (currentHospit.getLeft() != null) {
                    hospits.add(currentHospit.getLeft());
                }
                if (currentHospit.getRight() != null) {
                    hospits.add(currentHospit.getRight());
                }
            } else if (currentHospit.getRight() != null && curHospVal.getEnd().compareTo(d) < 0) {
                hospits.add(currentHospit.getRight());
            } else if (currentHospit.getLeft() != null && d.compareTo(curHospVal.getStart()) < 0) {
                hospits.add(currentHospit.getLeft());
            }
        }
        return actualPatients;
    }

    /**
     *
     * @param hName
     * @param d
     * @param ins
     * @return
     */
    public List showActualHospitPatientsSorted(String hName, Date d, int ins) {
        List<Hospitalization> actualPatients = new ArrayList<>();
        RBTree<String> sortedTree = new RBTree<>();
        Queue<RBTreeNode> hospits = new LinkedList<>();
        RBTreeNode hos = hospitalTree.find(hName);
        if (hos == null) {
            return null;
        }
        Hospital hosValue = (Hospital) hos.getValue(); //najdem nemocnicu
        RBTreeNode ic = hosValue.getInsuranceTree().find(ins);
        InsuranceCompany icVal = (InsuranceCompany) ic.getValue();
        hospits.add(icVal.getHospitalizationTree().getRoot());
        while (!hospits.isEmpty()) {
            RBTreeNode currentHospit = hospits.poll();
            Hospitalization curHospVal = (Hospitalization) currentHospit.getValue();
            if (curHospVal.getStart().compareTo(d) <= 0 && curHospVal.getEnd().compareTo(d) >= 0) {
                sortedTree.insert(new RBTreeNode(curHospVal.getPatientID(), curHospVal));
            }
            if (curHospVal.getStart().compareTo(d) <= 0 && d.compareTo(curHospVal.getEnd()) <= 0) {
                if (currentHospit.getLeft() != null) {
                    hospits.add(currentHospit.getLeft());
                }
                if (currentHospit.getRight() != null) {
                    hospits.add(currentHospit.getRight());
                }
            } else if (currentHospit.getRight() != null && curHospVal.getEnd().compareTo(d) < 0) {
                hospits.add(currentHospit.getRight());
            } else if (currentHospit.getLeft() != null && d.compareTo(curHospVal.getStart()) < 0) {
                hospits.add(currentHospit.getLeft());
            }
        }
        actualPatients = sortedTree.inOrderRoot();
        return actualPatients;
    }

    /**
     *
     * @param hName
     * @param start
     * @param end
     * @return
     */
    public List showPatientsRange(String hName, Date start, Date end) {
        List<Hospitalization> actualPatients = new ArrayList<>();
        Queue<RBTreeNode> hospits = new LinkedList<>();
        RBTreeNode hos = hospitalTree.find(hName);
        if (hos == null) {
            return null;
        }
        Hospital hosValue = (Hospital) hos.getValue(); //najdem nemocnicu
        hospits.add(hosValue.getHospitTree().getRoot());
        while (!hospits.isEmpty()) {
            RBTreeNode currentHospit = hospits.poll();

            Hospitalization curHospVal = (Hospitalization) currentHospit.getValue();
            if ((start.compareTo(curHospVal.getStart()) <= 0 && end.compareTo(curHospVal.getStart()) >= 0)
                    || (start.compareTo(curHospVal.getEnd()) <= 0 && end.compareTo(curHospVal.getEnd()) >= 0)
                    || (start.compareTo(curHospVal.getStart()) >= 0 && end.compareTo(curHospVal.getEnd()) <= 0)) {
                actualPatients.add(curHospVal);
            }
            if (curHospVal.getEnd().compareTo(start) < 0 && curHospVal.getStart().compareTo(start) < 0) {
                if (currentHospit.getRight() != null) {
                    hospits.add(currentHospit.getRight());
                }
            } else if (end.compareTo(curHospVal.getStart()) < 0 && end.compareTo(curHospVal.getEnd()) < 0) {
                if (currentHospit.getLeft() != null) {
                    hospits.add(currentHospit.getLeft());
                }
            } else {
                if (currentHospit.getRight() != null) {
                    hospits.add(currentHospit.getRight());
                }
                if (currentHospit.getLeft() != null) {
                    hospits.add(currentHospit.getLeft());
                }
            }
        }
        return actualPatients;
    }
    
    public String deleteHospital(String del, String n){
        RBTreeNode delete = hospitalTree.find(del);
        if (delete == null) {
            return "Deleted hospital not found";
        }
        Hospital delValue = (Hospital) delete.getValue(); //najdem nemocnicu
        
        RBTreeNode alter = hospitalTree.find(n);
        if (alter == null) {
            return "Alternative hospital not found";
        }
        Hospital alterValue = (Hospital) alter.getValue(); //najdem nemocnicu
        
        while (delValue.getPatientTree1().getRoot()!=null){
            RBTreeNode cur=delValue.getPatientTree1().getRoot();
            Patient curVal=(Patient) cur.getValue();
            String key=curVal.getFirstName()+curVal.getSurname()+curVal.getIDnumber();
            RBTreeNode pat=new RBTreeNode(curVal.getIDnumber(), curVal);
            RBTreeNode mat=new RBTreeNode(key, curVal);
            alterValue.getPatientTree1().insert(pat);
            alterValue.getPatientTree2().insert(mat);
            delValue.getPatientTree1().delete((String) delValue.getPatientTree1().getRoot().getKey());
        }
        
        for (int i=1;i<=3;i++){
            RBTreeNode ic = delValue.getInsuranceTree().find(i);
            InsuranceCompany icVal = (InsuranceCompany) ic.getValue();
            while(icVal.getHospitalizationTree().getRoot()!=null){
                RBTreeNode hosp=icVal.getHospitalizationTree().getRoot();
                Hospitalization hospVal=(Hospitalization) hosp.getValue();
                StartEndID key = (StartEndID) hosp.getKey();
                hospVal.setHospitalName(alterValue.getName());
                alterValue.getHospitTree().insert(new RBTreeNode(key, hospVal));
                RBTreeNode icAlter = alterValue.getInsuranceTree().find(i);
                InsuranceCompany icAlterVal = (InsuranceCompany) icAlter.getValue();
                icAlterVal.getHospitalizationTree().insert(new RBTreeNode(key, hospVal));
                
                icVal.getHospitalizationTree().delete(key);
            }
            
        }
        
        hospitalTree.delete(del);
        return "Success!";
    }

    /**
     *
     * @param start
     * @param end
     * @param insNumber
     * @return
     */
    public List bills(Date start, Date end, int insNumber) {
        RBTree<DateID> tree = new RBTree<>();
        counts = new ArrayList<>();
        Queue<RBTreeNode> hospits = new LinkedList<>();
        for (int i = 1; i <= insNumber; i++) {
            int counter = 0;
            RBTreeNode insCom = insuranceTree.find(i);
            InsuranceCompany insComVal = (InsuranceCompany) insCom.getValue();

            hospits.add(insComVal.getHospitalizationTree().getRoot());
            while (!hospits.isEmpty()) {
                RBTreeNode currentHospit = hospits.poll();
                Date startD = null;
                Date endD = null;

                Hospitalization curHospVal = (Hospitalization) currentHospit.getValue();
                if (curHospVal.getEnd().compareTo(start) <= 0 || end.compareTo(curHospVal.getStart()) < 0) {
                }
                if (curHospVal.getStart().compareTo(start) <= 0 && start.compareTo(curHospVal.getEnd()) <= 0 && curHospVal.getEnd().compareTo(end) <= 0) {
                    startD = start;
                    endD = curHospVal.getEnd();
                }
                if (start.compareTo(curHospVal.getStart()) <= 0 && curHospVal.getStart().compareTo(end) <= 0 && curHospVal.getEnd().compareTo(end) <= 0) {
                    startD = curHospVal.getStart();
                    endD = curHospVal.getEnd();
                }
                if (start.compareTo(curHospVal.getStart()) <= 0 && curHospVal.getStart().compareTo(end) <= 0 && end.compareTo(curHospVal.getEnd()) <= 0) {
                    startD = curHospVal.getStart();
                    endD = end;
                }
                if (curHospVal.getStart().compareTo(start) <= 0 && end.compareTo(curHospVal.getEnd()) < 0) {
                    startD = start;
                    endD = end;
                }

                if (startD != null && endD != null) {
                    Calendar startCal = Calendar.getInstance();
                    startCal.setTime(startD);
                    int startDay = startCal.get(Calendar.DAY_OF_MONTH);

                    Calendar endCal = Calendar.getInstance();
                    startCal.setTime(endD);
                    int endDay = startCal.get(Calendar.DAY_OF_MONTH);

                    int diff = endDay - startDay;
                    counter += diff;
                    for (int j = startDay; j <= endDay; j++) {
                        Date date = new Date(startD.getYear(), startD.getMonth(), j);
                        Bill b = new Bill(date, curHospVal.getPatientID(), curHospVal.getHospitalName(), curHospVal.getDiagnosis());
                        DateID key = new DateID(date, curHospVal.getPatientID());
                        RBTreeNode bill = new RBTreeNode(key, b);
                        tree.insert(bill);
                    }

                }
                if (curHospVal.getEnd().compareTo(start) < 0 && curHospVal.getStart().compareTo(start) < 0) {
                    if (currentHospit.getRight() != null) {
                        hospits.add(currentHospit.getRight());
                    }
                } else if (end.compareTo(curHospVal.getStart()) < 0 && end.compareTo(curHospVal.getEnd()) < 0) {
                    if (currentHospit.getLeft() != null) {
                        hospits.add(currentHospit.getLeft());
                    }
                } else {
                    if (currentHospit.getRight() != null) {
                        hospits.add(currentHospit.getRight());
                    }
                    if (currentHospit.getLeft() != null) {
                        hospits.add(currentHospit.getLeft());
                    }
                }
            }
            counts.add(counter);
        }
        return tree.inOrderRoot();
    }

    /**
     *
     * @return
     */
    public String save() {
        CsvWriter.writePatients(patientTree);
        CsvWriter.writeHospitals(hospitalTree);
        CsvWriter.writeHospits(insuranceTree);
        return "Saving done!";
    }

    /**
     *
     * @return
     */
    public String load() {
        patientTree = new RBTree<>();
        hospitalTree = new RBTree<>();
        insuranceTree = new RBTree<>();

        CsvReader csv = new CsvReader();
        csv.readPatients();
        csv.readHospitals();
        csv.readHospits();

        patientTree = csv.getPatientTree();
        hospitalTree = csv.getHospitalTree();
        insuranceTree = csv.getInsuranceTree();

        return "Loading done!";
    }

    /**
     *
     * @return
     */
    public List fillHospitalsSortedByName() {
        return hospitalTree.inOrderRoot();
    }
    
    /**
     *
     * @return
     */
    public List fillHospitalsPostOrder() {
        return hospitalTree.postOrderRoot();
    }

    /**
     *
     * @return
     */
    public RBTree<String> getHospitalTree() {
        return hospitalTree;
    }

    /**
     *
     * @param hospitalTree
     */
    public void setHospitalTree(RBTree<String> hospitalTree) {
        this.hospitalTree = hospitalTree;
    }

    /**
     *
     * @return
     */
    public RBTree<String> getPatientTree() {
        return patientTree;
    }

    /**
     *
     * @param patientTree
     */
    public void setPatientTree(RBTree<String> patientTree) {
        this.patientTree = patientTree;
    }

    /**
     *
     * @return
     */
    public RBTree<Integer> getInsuranceTree() {
        return insuranceTree;
    }

    /**
     *
     * @param insuranceTree
     */
    public void setInsuranceTree(RBTree<Integer> insuranceTree) {
        this.insuranceTree = insuranceTree;
    }

    /**
     *
     * @return
     */
    public List<Integer> getCounts() {
        return counts;
    }

    /**
     *
     * @param counts
     */
    public void setCounts(List<Integer> counts) {
        this.counts = counts;
    }

}

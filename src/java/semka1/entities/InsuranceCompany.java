/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semka1.entities;

import semka1.RBTree.RBTree;

/**
 *
 * @author peter
 */
public class InsuranceCompany {
    private int insID;
    private RBTree<StartEndID> hospitalizationTree=new RBTree<>();
    
    /**
     *
     */
    public InsuranceCompany(){}

    /**
     *
     * @param insID
     */
    public InsuranceCompany(int insID) {
        this.insID = insID;
    }

    /**
     *
     * @return
     */
    public int getInsID() {
        return insID;
    }

    /**
     *
     * @param insID
     */
    public void setInsID(int insID) {
        this.insID = insID;
    }

    /**
     *
     * @return
     */
    public RBTree<StartEndID> getHospitalizationTree() {
        return hospitalizationTree;
    }

    /**
     *
     * @param hospitalizationTree
     */
    public void setHospitalizationTree(RBTree<StartEndID> hospitalizationTree) {
        this.hospitalizationTree = hospitalizationTree;
    }

    
}

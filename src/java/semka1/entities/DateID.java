/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semka1.entities;

import java.util.Date;

/**
 *
 * @author peter
 */
public class DateID implements Comparable<DateID> {

    private Date date;
    private String patientID;

    /**
     *
     * @param d
     * @param ID
     */
    public DateID(Date d, String ID) {
        this.date = d;
        this.patientID = ID;
    }


    @Override
    public int compareTo(DateID d) {
        int sDate = this.date.compareTo(d.date);
        if (sDate<0 || sDate>0) {
            return sDate;
        } 
            else if (sDate==0) sDate=-1;
        
        return sDate;
    }

    /**
     *
     * @return
     */
    public String getPatientID() {
        return patientID;
    }

    /**
     *
     * @param patientID
     */
    public void setPatientID(String patientID) {
        this.patientID = patientID;
    }

    /**
     *
     * @return
     */
    public Date getDate() {
        return date;
    }

    /**
     *
     * @param date
     */
    public void setDate(Date date) {
        this.date = date;
    }
    
    

}

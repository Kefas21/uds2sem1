/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semka1.entities;

import semka1.RBTree.RBTree;

/**
 *
 * @author peter
 */
public class Hospital {
    private String name;
    private RBTree<Integer> insuranceTree=new RBTree<>(); //poistovne podla kodu
    private RBTree<String> patientTree1=new RBTree<>(); //pacienti zoradeni podla RC
    private RBTree<String> patientTree2=new RBTree<>(); //pacienti zoradeni podla mena, priezviska, RC 
    private RBTree<StartEndID> hospitTree=new RBTree<>(); //hospitalizacie podla start+end+id
    
    /**
     *
     */
    public Hospital(){}

    /**
     *
     * @param name
     */
    public Hospital(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     */
    public RBTree<String> getPatientTree1() {
        return patientTree1;
    }

    /**
     *
     * @param patientTree1
     */
    public void setPatientTree1(RBTree<String> patientTree1) {
        this.patientTree1 = patientTree1;
    }

    /**
     *
     * @return
     */
    public RBTree<Integer> getInsuranceTree() {
        return insuranceTree;
    }

    /**
     *
     * @param insuranceTree
     */
    public void setInsuranceTree(RBTree<Integer> insuranceTree) {
        this.insuranceTree = insuranceTree;
    }

    /**
     *
     * @return
     */
    public RBTree<String> getPatientTree2() {
        return patientTree2;
    }

    /**
     *
     * @param patientTree2
     */
    public void setPatientTree2(RBTree<String> patientTree2) {
        this.patientTree2 = patientTree2;
    }

    /**
     *
     * @return
     */
    public RBTree<StartEndID> getHospitTree() {
        return hospitTree;
    }

    /**
     *
     * @param hospitTree
     */
    public void setHospitTree(RBTree<StartEndID> hospitTree) {
        this.hospitTree = hospitTree;
    }  
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semka1.entities;

import java.util.Date;

/**
 *
 * @author peter
 */
public class StartEndID implements Comparable<StartEndID> {

    private Date startDate;
    private Date endDate;
    private String patientID;

    /**
     *
     * @param start
     * @param end
     * @param ID
     */
    public StartEndID(Date start, Date end, String ID) {
        this.startDate = start;
        this.endDate = end;
        this.patientID = ID;
    }

    /**
     *
     * @return
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     *
     * @param startDate
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     *
     * @return
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     *
     * @param endDate
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public int compareTo(StartEndID d) {
        int sDate = this.startDate.compareTo(d.startDate);
        if (sDate<0 || sDate>0) {
            return sDate;
        } else if (sDate == 0) {
            sDate = this.endDate.compareTo(d.endDate);
            if (sDate<0 || sDate>0) {
                return sDate;
            }
            else if (sDate==0) sDate=-1;
        }
        return sDate;
    }

    /**
     *
     * @return
     */
    public String getPatientID() {
        return patientID;
    }

    /**
     *
     * @param patientID
     */
    public void setPatientID(String patientID) {
        this.patientID = patientID;
    }

}
